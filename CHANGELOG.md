# v0.8.1

  * Fix compilation failure with `serde_yml` feature.

# v0.8.0 (yanked)

  * Support `serde_yml`. This is a fork of `serde_yaml` which has a maintenance
    future.
  * Update to `yaml-rust2` version 0.10.

# v0.7.0

  * MSRV for the minimal build is now 1.70. This is to migrate to `yaml-rust2`
    instead of `yaml-rust`.
  * Migrate to `yaml-rust2` as `yaml-rust` is unmaintained.

# v0.6.0

  * `serde_yaml` 0.9 is now supported. 0.8 is no longer supported.
  * Re-export `serde_yaml` to be able to use it from dependent crates to prevent dependency hell.

# v0.5.1

  * Properly transform `.inf` and `.nan` when using the `serde_yaml` feature.

# v0.5.0

  * MSRV bumped to 1.41. While the code update is just for a small code
    simplification, in order to simplify CI, updating makes sense.

# v0.4.1

  * Add usage documentation.

# v0.4.0

  * Remove `error-chain`.

# v0.3.0

  * Updated dependencies.

# v0.2.1

  * `docs.rs` now builds with the `serde_yaml` feature.

# v0.2.0

  * `serde_yaml-0.7` is now required.

# v0.1.0

  * Initial public release.
